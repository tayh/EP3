class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :nome
      t.integer :idade
      t.decimal :peso
      t.string :estado
      t.string :cidade

      t.timestamps null: false
    end
  end
end
