Rails.application.routes.draw do
  devise_for :users
  resources :profiles, only: [:new, :create, :destroy, :edit, :update]
  get "/profiles/busca" => "profiles#busca", as: :busca_profile
  root "profiles#index"

end
