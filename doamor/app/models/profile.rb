class Profile < ActiveRecord::Base
  validates :nome, :idade, :peso, :estado, :cidade, presence: true
  belongs_to :user
end
