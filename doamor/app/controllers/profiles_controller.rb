class ProfilesController < ApplicationController
  before_action :authenticate_user!
  def index
    @profiles = Profile.all
  end

  def new
    @profile = Profile.new
  end

  def create
    valores = params.require(:profile).permit :nome, :idade, :peso, :estado, :cidade
    @profile = Profile.new valores
    if @profile.save
      flash[:notice] = "Perfil salvo com sucesso"
      redirect_to root_url
    else
      render :new
    end
  end

  def edit
    id = params[:id]
    @profile = Profile.find(id)
    render :edit
  end

  def update
    id = params[:id]
    @profile = Profile.find(id)
    valores = params.require(:profile).permit :nome, :idade, :peso, :estado, :cidade
    if @profile.update valores
      flash[:notice] = "Perfil atualizado com sucesso"
      redirect_to root_url
    else
      render :edit
    end
  end

  def destroy
    id = params[:id]
    Profile.destroy id
    redirect_to root_url
  end

  def busca
    @nome = params[:nome]
    @profiles = Profile.where "nome like ?", "%#{@nome}%"
  end
end
